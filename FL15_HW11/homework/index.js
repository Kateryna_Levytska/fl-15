function isEquals(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a > b;
}

function storeNames() {
    return [...arguments];
}

function getDifference(a, b) {
    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }
}

function negativeCount(arr) {
    return arr.reduce((acc, val) => {
        if (val < 0) {
            return ++acc;
        } else {
            return acc;
        }
    }, 0);
}

function letterCount(str, char) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === char) {
            count++;
        }
    }
    return count;
}

function countPoints(arr) {
    let count = 0;
    const three = 3;
    arr.forEach(el => {
        const newArr = el.split(':');
        if (+newArr[0] > +newArr[1]) {
            count += three;
        } else if (+newArr[0] === +newArr[1]) {
            count++;
        }
    });
    return count;
}