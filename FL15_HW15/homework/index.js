/* START TASK 1: Your code goes here */
const table = document.querySelector('table');
const two = 2;
const three = 3;
const specialCell = table.rows[1].cells[two];
table.addEventListener('click', function(e) {
    let curRow = -1;
    let curCell = -1;
    for (let i = 0; i < three; i++) {
        for (let j = 0; j < three; j++) {
            if (e.target === table.rows[i].cells[j]) {
                curRow = i;
                curCell = j;
            }
        }
    }
    if (curCell !== 0 && e.target !== specialCell) {
        e.target.classList.remove('blue');
        e.target.classList.add('yellow');
    } else if (curCell === 0) {
        for (let i = 0; i < three; i++) {
            if (!table.rows[curRow].cells[i].classList.contains('yellow')) {
                table.rows[curRow].cells[i].classList.add('blue');
            }
        }
    } else if (e.target === specialCell) {
        table.classList.remove('green');
        table.classList.add('yellow');
        for (let i = 0; i < three; i++) {
            for (let j = 0; j < three; j++) {
                if (!table.rows[i].cells[j].classList.contains('yellow') &&
                !table.rows[i].cells[j].classList.contains('blue') ) {
                    table.rows[curRow].cells[i].classList.add('yellow');
                }
            }
        }
    } else {
        e.preventDefault();
    }
});

/* END TASK 1 */

/* START TASK 2: Your code goes here */
const nine = 9;
const thirteen = 13;
const phoneNumber = document.querySelector('#phoneNumber');
const sendButton = document.querySelector('#sendButton');
const divMessageError = document.querySelector('#messageError');
const divMessageSuccess = document.querySelector('#messageSuccess');

function isValid(value) {
    const arr = value.split('');
    if (arr[0] !== '+' || arr.length !== thirteen || !arr) {
        return false;
    }
    for (let i = 1; i < thirteen; i++) {
        if (!(arr[i] >= 0 && arr[i] <= nine)) {
            return false;
        }
    }
    return true;
}

phoneNumber.addEventListener('change', function() {
    divMessageSuccess.classList.add('hidden');
    if (isValid(this.value)) {
        divMessageError.classList.add('hidden');
    } else {
        divMessageError.classList.remove('hidden');
    }
    if (isValid(this.value)) {
        sendButton.removeAttribute('disabled');
    } else {
        sendButton.setAttribute('disabled', 'disabled');
    }
});

sendButton.addEventListener('click', function() {
    divMessageSuccess.classList.remove('hidden');
});

/* END TASK 2 */

/* START TASK 3: Your code goes here */
const ball = document.querySelector('#ball');
const court = document.querySelector('#court');
court.onclick = function(event) {
    let fieldCoords = this.getBoundingClientRect();
    let ballCoords = {
      top: event.clientY - fieldCoords.top - court.clientTop - ball.clientHeight / two,
      left: event.clientX - fieldCoords.left - court.clientLeft - ball.clientWidth / two
    };
    if (ballCoords.top < 0) {
        ballCoords.top = 0;
    }
    if (ballCoords.left < 0) {
        ballCoords.left = 0;
    }
    if (ballCoords.left + ball.clientWidth > court.clientWidth) {
      ballCoords.left = court.clientWidth - ball.clientWidth;
    }
    if (ballCoords.top + ball.clientHeight > court.clientHeight) {
      ballCoords.top = court.clientHeight - ball.clientHeight;
    }
    ball.style.left = ballCoords.left + 'px';
    ball.style.top = ballCoords.top + 'px';
  }
/* END TASK 3 */
