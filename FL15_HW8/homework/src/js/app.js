const meeting = prompt('Input event name', 'meeting');

const form = document.forms[0];

const submit = document.getElementById('confirm');
const convert = document.getElementById('converter');

if(meeting) {
    form.hidden = false;
}

submit.addEventListener('click', e => {
    e.preventDefault();

    const userName = form.elements.name.value;
    const time = form.elements.time.value;
    const place = form.elements.place.value;
    const timeRegExp = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
    const validTime = timeRegExp.test(time);

    if (userName === '' || time === '' || place === '') {
        alert('Input all data');
    } else if (!validTime) {
        alert('Enter time in format hh:mm');
    } else {
        console.log(`${userName} has a ${meeting} today at ${time} somewhere in ${place}`)
    }
});

convert.addEventListener('click', e => {
    e.preventDefault();

    const euros = +prompt('Input amount of euros', '');
    const dollars = +prompt('Input amount of dollars', '');

    const euroHrn = 32.98;
    const dollarHrn = 27.68;

    const two = 2;

    if (isValidNumber(euros) && isValidNumber(dollars)) {
        alert(`${euros.toFixed(two)} euros are equal ${(euros * euroHrn).toFixed(two)}hrns,
${dollars.toFixed(two)} dollars are equal ${(dollars * dollarHrn).toFixed(two)}hrns`);
    }
});

function isValidNumber(num) {
    return !isNaN(parseFloat(num)) && isFinite(num) && num > 0;
}