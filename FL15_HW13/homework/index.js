function getAge(date){
    const currentDay = new Date();
    const birthday = new Date(date);
    if (currentDay.getMonth() < birthday.getMonth() || 
    currentDay.getMonth() === birthday.getMonth() && currentDay.getDate() < birthday.getDate()) {
        return currentDay.getFullYear() - birthday.getFullYear() - 1
    } else {
        return currentDay.getFullYear() - birthday.getFullYear()
    }
}

function getWeekDay(date) {
    const daysOfTheWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const newDate = new Date(date);
    return daysOfTheWeek[newDate.getDay()];
}

function getAmountDaysToNewYear() {
    const currentDay = new Date();
    const milisecInDay = 86400000;
    const newYear = new Date(currentDay.getFullYear() + 1, 0, 1);
    return Math.ceil((newYear - currentDay) / milisecInDay);
}

function getProgrammersDay(year) {
    const yearOfDate = new Date(year, 0, 1);
    const fourHundred = 400;
    const hundred = 100;
    const four = 4;
    const eight = 8;
    const twelve = 12;
    const thirteen = 13;
    if (year % fourHundred === 0 || year % hundred !== 0 && year % four === 0) {
        return `12 Sep, ${yearOfDate.getFullYear()} (${getWeekDay(new Date(year, eight, twelve))})`;
    } else {
        return `13 Sep, ${yearOfDate.getFullYear()} (${getWeekDay(new Date(year, eight, thirteen))})`;
    }
}

function howFarIs(str) {
    const week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const specifiedWeekday = str.charAt().toUpperCase() + str.slice(1).toLowerCase();
    const currentDay = new Date();
    const currentDayIndex = currentDay.getDay();
    const seven = 7;
    const specifiedIndex = week.findIndex(el => el === specifiedWeekday);
    const number = currentDayIndex < specifiedIndex ? 
    specifiedIndex - currentDayIndex : specifiedIndex - currentDayIndex + seven;
    if (currentDayIndex === specifiedIndex) {
        return `Hey, today is ${specifiedWeekday} =)`;
    } else {
        return `It's ${number} day(s) left till ${specifiedWeekday}`;
    }
}

function isValidIdentifier(str) {
    const regExp = /[a-zA-Z_$][\w$]*/g;
    const arrayOfStr = str.match(regExp);
    return arrayOfStr ? arrayOfStr[0] === str : false;
}

function capitalize(str) {
    return str.replace(/\b\w/gi, str => str[0].toUpperCase());
}

function isValidAudioFile(str) {
    const regExp = /[a-zA-Z]+\.(mp3|flac|alac|aac)/;
    const arrayOfStr = str.match(regExp);
    return arrayOfStr ? arrayOfStr[0] === str : false;
}

function getHexadecimalColors(str) {
    const regExp = /#([0-9a-f]{3}|[0-9a-f]{6})\b/gi;
    const arrayOfStr = str.match(regExp);
    return arrayOfStr || [];
}

function isValidPassword(str) {
    const negOne = -1;
    const reg = /[a-zA-Z0-9]{8,}/g;
    const arrayOfStr = str.match(reg);
    const ifMatch = arrayOfStr ? arrayOfStr[0] === str : false;
    if (str.search(/[a-z]/) === negOne || str.search(/[A-Z]/) === negOne || 
    str.search(/\d/) === negOne || !ifMatch) {
        return false;
    } else {
        return true;
    }
}

function addThousandsSeparators(number) {
return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function getAllUrlsFromText(text) {
    const regExp = /(http|https):\/\/[a-z0-9]+(\.[a-z0-9]+)+\//gi;
    const arrayOfUrl = text.match(regExp);
    return arrayOfUrl || [];
}