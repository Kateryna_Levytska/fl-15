function reverseNumber(num) {
    const str = num.toString();
    let newStr = '';
    for (let i = str.length - 1; i >= 0; i--) {
        newStr += str[i];
    }
    if (newStr[newStr.length - 1] === '-') {
        newStr = '-' + newStr.slice(0, newStr.length - 1);
    }
    return +newStr
}

function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}

function map(arr, func) {
    const newArr = [];
    forEach(arr, el => {
        newArr.push(func(el));
    });
    return newArr;    
}

function filter(arr, func) {
    const filteredArr = [];
    forEach(arr, el => {
        if (func(el)) {
            filteredArr.push(el);
        }
    }); 
    return filteredArr;
}

function getAdultAppleLovers(data) {
    const result = [];
    const adult = 18;
    filter(data, el => {
        for (let key1 in el) {
            if (data.hasOwnProperty(key1) && key1 === 'age' && el[key1] > adult) {
                for (let key2 in el) {
                    if (data.hasOwnProperty(key2) && key2 === 'favoriteFruit' && el[key2] === 'apple') {
                        result.push(el.name);
                    }  
                }
            }
        }
    });
    return result;
}

function getKeys(obj) {
    const arrOfKeys = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            arrOfKeys.push(key);
        }
    }
    return arrOfKeys;
}

function getValues(obj) {
    const arrOfValues = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            arrOfValues.push(obj[key]);
        }
    }
    return arrOfValues;
}

function showFormattedDate(dateObj) {
    const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return `It is ${dateObj.getDate()} of ${month[dateObj.getMonth()]}, ${dateObj.getFullYear()}`
}
