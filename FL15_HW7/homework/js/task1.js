const maxPercentage = 100;
const numbersAfterComma = 2;
const minIntialAmount = 1000;
const intialAmount = parseInt(prompt('Please input initial amount of money', ''));
const years = parseInt(prompt('Please input number of years', ''));
const percentage = parseInt(prompt('Please input percentage of a year', ''));
const totalAmount = (intialAmount * Math.pow(1 + percentage/maxPercentage, years)).toFixed(numbersAfterComma);
const totalProfit = (totalAmount - intialAmount).toFixed(numbersAfterComma);
if (!Number.isFinite(intialAmount) || !Number.isFinite(years) || !Number.isFinite(percentage) || 
intialAmount < minIntialAmount || years < 1 || percentage > maxPercentage) {
    alert('Invalid input data');
} else {
alert(`Intial amount: ${intialAmount}
Number of years: ${years}
Percentage of year: ${percentage}
Total profit: ${totalProfit}
Total amount: ${totalAmount}`);
}
