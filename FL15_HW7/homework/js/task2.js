let playGame = confirm('Do you want to play a game?');
if (!playGame) {
    alert('You did not become a billionaire, but can.');
}

const numberRangeIncrement = 4;
const prizeIncrement = 2;
const prizeDecrement = 2;
const startNumberRange = 8;
const startPrize = 100;
let currentNumberRange;
let currentGamePrize;
let currentRoundPrize;
let totalPrize = 0;
let isFirstRound = true;

while (playGame) {
    let attempts = 3;
    let isNumberGuessed = false;
    if (isFirstRound) {
        currentGamePrize = startPrize;
        currentNumberRange = startNumberRange;
    } else {
        currentGamePrize *= prizeIncrement;
        currentNumberRange += numberRangeIncrement;
    }
    let luckyNumber = Math.round(Math.random() * currentNumberRange);
    currentRoundPrize = currentGamePrize;
    while (attempts > 0) {
        let userGuess = parseInt(prompt(
`Choose a roulette pocket number from 0 to ${currentNumberRange}
Attempts left: ${attempts}
Total prize: ${totalPrize}
Possible prize on current attempt: ${currentRoundPrize}`
        ));
        if (userGuess === luckyNumber) {
            totalPrize += currentRoundPrize;
            attempts = 0;
            let wantToContinue = confirm(
`Congratulation, you won! Your prize is: ${totalPrize}$.Do you want to continue?`
            );
            if (wantToContinue) {
                isNumberGuessed = true;
                isFirstRound = false;
            }
        } else {
            currentRoundPrize /= prizeDecrement;
            attempts--;
        }
    }
    if (!isNumberGuessed) {
        alert(`Thank you for your participation. Your prize is: ${totalPrize}$`);
        const playGameAgain = confirm('Do you want to play again?');
        if (!playGameAgain) {
            playGame = false;
        } else {
            isFirstRound = true;
        }
    }
}
alert('You did not become a billionaire, but can.');
