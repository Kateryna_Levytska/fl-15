function visitLink(path) {
	const visitCounters = getCounters();
	visitCounters[path]++;
	localStorage.setItem('visitCounters', JSON.stringify(visitCounters));
}

function viewResults() {
	const visitCounters = getCounters();
	const contentEl = document.getElementById('content');
	const resultsEl = document.createElement('ul');
	Object.keys(visitCounters).forEach(function(key) {
		const listItem = document.createElement('li');
		listItem.innerText = `You visited ${key} ${visitCounters[key]} time(s)`;
		resultsEl.appendChild(listItem);
	});
		contentEl.appendChild(resultsEl);

	localStorage.removeItem('visitCounters');
}


function getCounters() {
	let visitCounters = JSON.parse(localStorage.getItem('visitCounters'));
	if (!visitCounters) {
		visitCounters = {
			Page1: 0,
			Page2: 0,
			Page3: 0
		};
	}
	return visitCounters;
}
