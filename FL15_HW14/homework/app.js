const appRoot = document.getElementById('app-root');
function renderPage() {
    createElement('h1', appRoot, 'header', 'Countries Search');
    const form = createElement('form', appRoot, 'form');
    const searchTypeDiv = createElement('div', form, 'search-type');
    createElement('p', searchTypeDiv, null, 'Please choose the type of search:');
    const searchDiv = createElement('div', searchTypeDiv, 'search-div');
    const searchByRegionDiv = createElement('div', searchDiv, 'search-by-region-div');
    const searchByRegion = createElement('input', searchByRegionDiv);
    setAttributes(searchByRegion, {
        type: 'radio',
        value: 'by-region',
        id: 'byRegion',
        name: 'searchType'
    });
    createElement('label', searchByRegionDiv, null, 'By Region');
    searchByRegion.addEventListener('change', updateSearchQuery);
    const searchByLanguageDiv = createElement('div', searchDiv, 'search-by-language-div');
    const searchByLanguage = createElement('input', searchByLanguageDiv);
    setAttributes(searchByLanguage, {
        type: 'radio',
        value: 'by-language',
        id: 'byLanguage',
        name: 'searchType'
    });
    createElement('label', searchByLanguageDiv, null, 'By Language');
    searchByLanguage.addEventListener('change', updateSearchQuery);
    const searchQueryDiv = createElement('div', form, 'search-query');
    createElement('label', searchQueryDiv, null, 'Please choose search query:');
    const searchQuerySelect = createElement('select', searchQueryDiv, 'search-select', 'Select value');
    setAttributes(searchQuerySelect, {
        id: 'searchQuery',
        name: 'searchQuery',
        disabled: 'true'
    });
    addDefaultOption(searchQuerySelect);

    let countryList = [];

    searchQuerySelect.addEventListener('change', () => {
        if (document.querySelector('table')) {
            document.querySelector('table').remove();
        }
        if (searchQuerySelect.value) {
            document.querySelector('.no-items-message').classList.add('hidden');
            if (searchByRegion.checked) {
                countryList = externalService.getCountryListByRegion(searchQuerySelect.value);
            } else if (searchByLanguage.checked) {
                countryList = externalService.getCountryListByLanguage(searchQuerySelect.value);
            }
            createTable(appRoot, countryList);
        } else {
            document.querySelector('.no-items-message').classList.remove('hidden');
        }
    });

    createElement('p', appRoot, 'no-items-message', 'No items, please choose search query');
}

function createElement(tagName, parentElement, className, innerText) {
    const element = document.createElement(tagName);
    if (className) {
        element.classList.add(className);
    }
    if (innerText) {
        element.innerText = innerText;
    }
    parentElement.appendChild(element);
    return element;
}

function setAttributes(element, attributes) {
    Object.keys(attributes).forEach((key) => {
        element.setAttribute(key, attributes[key]);
    });
}

function updateSearchQuery() {
    const selectElement = document.getElementById('searchQuery');
    selectElement.removeAttribute('disabled');
    selectElement.innerHTML = '';
    const formData = new FormData(document.forms[0]);
    const searchType = formData.get('searchType');
    let options = [];
    addDefaultOption(selectElement);
    if (searchType === 'by-region') {
        options = externalService.getRegionsList();
    }
    if (searchType === 'by-language') {
        options = externalService.getLanguagesList();
    }
    options.forEach((option) => {
        const newOption = createElement('option', selectElement, null, option);
        newOption.setAttribute('value', option);
    });
}

function addDefaultOption(selectElement) {
    const defaultOption = createElement('option', selectElement, 'null', 'Select value');
    setAttributes(defaultOption, {
        value: '',
        selected: 'selected'
    });
}

function createTable(parentElement, list) {
    const table = createElement('table', parentElement, 'table');
    const thead = createElement('thead', table, 'thead');
    const thr = createElement('tr', thead);
    const tbody = createElement('tbody', table, 'tbody');

    const tableHeadList = ['Country name', 'Capital', 'World Region', 'Languages', 'Area', 'Flag'];

    tableHeadList.forEach(el => {
        createElement('th', thr, 'th-head', el);
    });

    list.forEach(el => {
        createRowTable(el);
    });

    function createRowTable(listElem) {
        const tr = createElement('tr', tbody, 'tr-body');
        createElement('td', tr, null, listElem.name);
        createElement('td', tr, null, listElem.capital);
        createElement('td', tr, null, listElem.region);
        createElement('td', tr, null, Object.values(listElem.languages).join(', '));
        createElement('td', tr, null, listElem.area);
        td = createElement('td', tr);
        img = createElement('img', td);
        setAttributes(img, { 'src': listElem.flagURL, 'alt': '' });
    }
}

renderPage();